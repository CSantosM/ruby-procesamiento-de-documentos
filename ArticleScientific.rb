class ArticleScientific < Article
  
  def initialize(journal,id,year,title,content,acronym)
    super(id,year,title,content,acronym)
    @journal = journal[1..-1]
  end

  def journal_equal(jour)
    return (@journal==jour)
  end

  def print_object()
    puts "Title: #{title} (#{year})"

    if content[0].length > 250 then
      puts "Abstract: #{content[0][0..250]}..."
    else
      puts "Abstract: #{content[0]}"
    end
    puts "Section number: #{sections.length()}"
    print "Sections: "
    sections.each(){|sec| puts sec}
  end

end