class Article
  require 'Acronym'

  def initialize(id,year,title,content,acronym)
    @id = id
    @year = year
    @title = title
    @content = content
    @sections = get_sections()
    @acronymObject = acronym
  end

  def list_title(year2)
    if (year == year2)
      return title
    end
  end

  def id_equal(id2)
    return id == id2.to_i
  end

  def equal_year(year2)
    return year == year2
  end

  def journal_equal(jour)

  end

  def get_sections()

    sections = []
    for i in (0..content.length()-1)
      if content[i].chomp == "--" then
        if content[i+1] != nil
          sections << content[i+1]
        end
      end
    end
    return sections

  end

  def print_object()
  end

  attr_reader :year, :title, :id, :acronymObject, :journal, :content, :sections

end
