class ArticlesCollection
  require 'Article'

  def initialize()
    @articlesArray = []

  end

  def addArticle(article)
    @articlesArray << article
    @articlesArray.sort!{|a,b| a.title <=> b.title}

  end

  #Enunciado 1
  def list_title_sort(year)
    titles = []
    articlesArray.each(){|art| titles << art.list_title(year)}
    if titles.compact!().empty?
      return "No se encuentran articlos publicados en el año #{year}\n "
    else
      return titles.sort
    end
  end

  #Enunciado 2
  def list_alljournal()
    journal = []
    articlesArray.each(){|art| journal << art.journal}
    return journal.compact!()
  end

  #Enunciado 3
  def title_with_acronym(acronym)
    articlesArray.each(){|art| puts art.title if art.acronymObject.acronyms.include?("("+acronym+")")}
  end

  #Enunciado 4
  def title_journal_acronym(jour,acronym)
    articlesArray.each(){|art| puts art.title if (art.journal_equal(jour)&&(art.acronymObject.acronyms.include?("("+acronym+")")))}

  end

  #Enunciado 5
  def acronyms_expanded(year)
    articlesArray.each(){|art| art.acronymObject.show_acrExpanded() if art.equal_year(year)}
  end

  #Enunciado 6
  def acronyms_id(id)
    articlesArray.each(){|art| art.acronymObject.show()  if art.id_equal(id)}
  end

  #Enunciado 7
  def list_without_acronym()
    articlesArray.each(){|art| puts("Titulo: "+art.title+" ID: "+art.id.to_s) if art.acronymObject.acronyms.empty?()}
  end

  #Enunciado 8
  def print_resume(acronym)
    articlesArray.each(){|art| art.print_object() if art.acronymObject.acronyms.include?("("+acronym+")")}
  end

  #Enunciado 9
  def group()
    all = articlesArray
    acr = []
    groups = []

    #creo una lista de todos los acronimos de todos los ficheros
    #agrupo los acronimos iguales en una lista y los ordeno por numero de apariciones
    articlesArray.each{|art| acr += art.acronymObject.acronyms}
    acrAll = acr.group_by{|a| a[/[a-zA-Z]+/] }.values
    acrAll.sort!{|a,b| b.length <=> a.length}

    # Agrupo los articulos en base a los 3 primeros acronimos mas repetidos en toda la coleccion
    cluster1 = groupBy(all,acrAll[0][0])
    all = articlesArray - cluster1
    cluster2 = groupBy(all,acrAll[1][0])
    all = articlesArray - (cluster1 + cluster2)
    cluster3 = groupBy(all,acrAll[2][0])
    all = articlesArray - (cluster1 + cluster2 + cluster3)

    puts "Cluster 1"
    cluster1.each(){|art| puts "#{art.id} - #{art.title}"}
    puts
    puts "Cluster 2"
    cluster2.each(){|art| puts "#{art.id} - #{art.title}"}
    puts
    puts "Cluster 3"
    cluster3.each(){|art| puts "#{art.id} - #{art.title}"}
    puts
    puts "Otros"
    all.each(){|art| puts "#{art.id} - #{art.title}"}

    groups << cluster1 << cluster2 << cluster3 << all

    return groups

  end

  #Enunciado 10
  def analytic(groups)
    as_number = 0.0
    aw_number = 0.0
    yes = 0
    sameDate = 0
    notSameDate = 0
    one_element = 0

    puts "Numero de grupos: #{groups.length}"
    puts "Numero medio de articulos por grupo: #{articlesArray.length()/groups.length()}"
    articlesArray.each(){|art| as_number+=1 if art.is_a? ArticleScientific}
    puts "Numero medio de artículos científicos por grupo es: #{as_number/groups.length()}"
    articlesArray.each(){|art| aw_number+=1 if art.is_a? ArticleWikipedia}
    puts "Numero medio de artículos wikipedia por grupo es: #{aw_number/groups.length()}"
    groups.each(){|item|
      if !item.empty? then
        date = item[0].year
      end
      item.each(){|art| yes+=1 if art.year==date}
      if yes == item.length() then
        sameDate +=1
      else
        notSameDate +=1
      end

    }
    puts "El numero de grupos con articulos de fechas iguales es: #{sameDate}"
    puts "El numero de grupos con articulos de fechas diferentes es: #{notSameDate}"
    groups.each(){|item| one_element+=1 if item.length()==1}
    puts "El numero de grupos con solo un elemento es: #{one_element}"
  end

  attr_reader :articlesArray

  private

  def groupBy(articles,acronym)
    cluster = []
    articles.each(){|art| cluster << art if art.acronymObject.acronyms.include?(acronym)}
    return cluster
  end

end