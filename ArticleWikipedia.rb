class ArticleWikipedia < Article
  
  def initialize(id,year,title,content,acronym)
    super(id,year,title,content,acronym)
  end

  def print_object()
    puts "Title: #{title} (#{year})"
    if content[1].length > 250 then
      puts "Introduction: #{content[1][0..250]}..."
    else
      puts "Introduction: #{content[1]}"
    end

    puts "Section number: #{sections.length()}"
    print "Sections: "
    sections.each(){|sec| puts sec}
  end

  def get_sections()
    sections = super()
    sections.unshift(content[0])
    return sections
  end
end