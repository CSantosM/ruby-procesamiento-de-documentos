class Acronym
  require 'Article'

  def initialize(acronym,expand,ntimes)
    @acronym = acronym
    @expanded = expand
    @ntimes = ntimes
  end

  def find_acronym(content)
    regular = /\([A-Z]+\)/
    str = ""
    list_acronym = []
    list_words = []
    i = 0
    #convierto el contenido del articulo a una lista de palabras
    content.each(){|element| str += element}
    list_words = str.split

    #creo una lista con acronimos y sus posiciones respectivas que ocupan en la lista de palabras
    list_words.each(){|word|
      if regular.match(word).to_s != "" then
        if regular.match(word).to_s.length > 3 then
          list_acronym << regular.match(word).to_s << i
        end
      end
      i+=1}

    return acronym_extra(list_acronym,list_words)

  end

  def show_acrExpanded()
    for i in (0..acronyms.length()-1)
      puts "#{acronyms[i]}" + " #{expanded[acronyms[i]]}"
    end
  end

  def show()
    if acronyms.empty? then
      puts "Este artículo no tiene acrónimos"
    else
      puts "Apariciones    Acrónimo"
      for i in (0.. acronyms.length()-1)
        print "#{ntimes[i]}  ..........  "
        puts "#{acronyms[i]}"
      end
    end
  end

  def acronyms
    return @acronym
  end

  def expanded
    return @expanded
  end

  def ntimes
    return @ntimes
  end

  private

  #creo una lista paralela a los acronimos con su forma expandida
  def acronym_extra(list_acronym,content)

    i=0
    ntimes = []
    expanded = []
    expand = Hash.new
    while list_acronym[i] != nil
      pos = list_acronym[i+1].to_i
      size = list_acronym[i].size-2
      expanded << content[pos-size..pos-1]

      i+=2

    end

    #borro las posiciones que ocupaban los acronimos en la lista de palabras
    for i in (0..list_acronym.length()-1)
      if is_number?(list_acronym[i]) then
        list_acronym.delete_at(i)
      end
    end

    #creo una tabla hash que por cada acronimo le pertenece su forma expandida.
    for i in (0..list_acronym.length()-1)
      if !expand.has_key?(list_acronym[i]) then
        expand[list_acronym[i]]= expanded[i]
        #puts expand["(AIS)"]
      end
    end

    list_acronym.each(){|acr|

      count = 0
      #cuento las aparaciones de cada acrónimo en el artículo
      for i in (0..content.length()-1)
        if (/#{acr[1..-2]}/.match(content[i]).to_s)!="" then
          count +=1
        end
      end

      ntimes << count

    }
    return Acronym.new(list_acronym,expand,ntimes)

  end

  def is_number? string
    true if Float(string) rescue false
  end

end

