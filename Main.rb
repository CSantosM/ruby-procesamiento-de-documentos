class Main
  require 'FileUtils'
  require 'Article'
  require 'ArticleScientific'
  require 'ArticlesCollection'
  require 'ArticleWikipedia'
  RUTA = 'ficheros/'
  EXT_CIENT = ".xml"
  EXT_WIKIP = ".txt"
  articles = []

  files = FileUtils.new
  file_list = files.list_files(RUTA) #contiene la lista de artchivos

  file_list.each(){|element| articles.push(files.read_file(element,EXT_CIENT))}
  #puts articles[1][0].shift
  acr = Acronym.new(nil,nil,nil)
  #Creo una lista con todos los Articulos
  collection = ArticlesCollection.new()
  for i in (0..articles.length()-1)
    if (File.extname(file_list[i]) == EXT_CIENT)
      journal = articles[i][0].shift
      id = articles[i][0].shift
      year = articles[i][0].shift
      title = (articles[i][0].shift(2)).last
      articles[i][0].shift
      content = articles[i][0]
      art = ArticleScientific.new(journal.chomp,id.to_i,year.to_i,title.chomp.to_s,content,acr.find_acronym(content))
      collection.addArticle(art)
      #list = art.show_acronym()
    else
      id = articles[i][0].shift
      year = articles[i][0].shift
      title = articles[i][0].shift
      articles[i][0].shift
      content = articles[i][0]
      art = ArticleWikipedia.new(id.chomp,year.to_i,title.chomp.to_s,content,acr.find_acronym(content))
      collection.addArticle(art)
    end

  end

  while(true)
    puts "////////////////////////////////////////////////////// MENU //////////////////////////////////////////////////////////"
    puts  "                                       Teclea una letra para elegir una opcion"
    puts  ""
    puts "1......Obtener titulos ordenados en un año determinado"
    puts "2......Obtener el listado de revistas"
    puts "3......Obtener titulos de articulos que contengan el acronimo determinado"
    puts "4......Obtener titulos de articulos publicados en una revista determinada que contengan un determinado acronimo"
    puts "5......Obtener listado de acronimos (y su forma expandida) de los articulos de un anio determinado"
    puts "6......Mostrar el listado de acronimos que contiene un articulo y el num de apariciones dado un identificador"
    puts "7......Mostrar titulos e identificadores de articulos que no contengan ningun acronimo"
    puts "8......Mostrar informacion de los articulos ordenados que contenga un acronimo determinado"
    puts "9......Agrupar los articulos"
    puts "10.....Calcular ciertas estadísticas relacionadas con los grupos de documentos"
    puts "0...... Salir del programa"
    puts ""
    puts "************************************************************************************************"
    puts "Bienvenido a Ruby, por favor elige una opcion"
    puts
    puts "-- IMPORTANTE: CREAR LOS GRUPOS DE ARTÍCULOS PARA PODER SELECCIONAR LA OPCIÓN DE ESTADISTICAS DE GRUPOS --"

    opcion = gets.chomp.to_i
    case opcion
    when 1
      puts "Introduce un año"
      year = gets.chomp.to_i
      puts collection.list_title_sort(year)

    when 2
      print "Listado de revistas: \n"
      puts collection.list_alljournal()
    when 3
      puts "Introduce un acrónimo en mayúsculas y sin paréntesis por favor"
      acronym = gets.chomp.to_s
      puts"Los titulos de los acronimos encontrados son:"
      collection.title_with_acronym(acronym)
    when 4
      puts "Introduce el nombre de una revista por favor"
      journal = gets.chomp.to_s
      puts "Introduce un acrónimo en mayúsculas y sin paréntesis por favor"
      acronym = gets.chomp.to_s
      collection.title_journal_acronym(journal,acronym)
    when 5
      puts "Introduce un año de publicación"
      year = gets.chomp.to_i
      collection.acronyms_expanded(year)
    when 6
      puts "Introduce el ID del articulo por favor"
      id = gets.chomp.to_s
      collection.acronyms_id(id)
    when 7

      collection.list_without_acronym()
    when 8
      puts "Introduce un acrónimo en mayúsculas y sin paréntesis por favor"
      acronym = gets.chomp.to_s
      collection.print_resume(acronym)
    when 9
      puts "Las agrupaciones quedan de la siguiente forma: "
      groups = collection.group()
    when 10
      puts "Las estadísticas de los grupos son:"
      collection.analytic(groups)
    when 0
      puts "Gracias, hasta la próxima"
      break

    else
      puts "Lo siento, la opción elegida no está disponible"
    end
  end
end
